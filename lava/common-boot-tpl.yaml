device_type: {{device_type}}
priority: medium
{% macro kernel_type(arch) -%}
    {% if arch == 'arm64' -%}
        image
    {%- else -%}
        zimage
    {%- endif %}
{%- endmacro %}

visibility: {{visibility}}

notify:
  callback:
    url: https://lavaphabbridge.apertis.org/
    method: POST
    dataset: results
    content-type: json
    token: lava-phab-bridge
  criteria:
    status: finished

timeouts:
  job:
    minutes: 180
  action:
    minutes: 120
  connections:
    overlay-unpack:
      minutes: 20

context:
  kernel_start_message: '.*'

metadata:
  source.project: '{{source_project}}'
  source.ref: '{{source_ref}}'
  source.commit: '{{source_commit}}'
  source.pipeline: '{{source_pipeline}}'
  source.job: '{{source_job}}'
  source.wip: {{source_wip}}
  image.version: '{{image_date}}'
  image.release: '{{release_version}}'
  image.arch: '{{arch}}'
  image.board: '{{board}}'
  image.type: '{{image_type}}'

actions:
  - deploy:
      namespace: flash
      timeout:
        minutes: 15
      to: tftp
      kernel:
        url: {{firststageurl}}/{{arch}}/nfs/vmlinuz
        type: {{kernel_type(arch)}}
      nfsrootfs:
        url: {{firststageurl}}/{{arch}}/nfs/apertis-nfs-{{arch}}.tar.gz
        compression: gz
      ramdisk:
        url: {{firststageurl}}/{{arch}}/nfs/initrd.img
        compression: gz
      os: debian
      {% if needs_dtb -%}
      dtb:
        url: {{firststageurl}}/{{arch}}/nfs/dtbs/{{firststagedtb}}
      {%- endif %}

  - boot:
      namespace: flash
      timeout:
        minutes: 30 # Minnowboards can spend a long time clearing memory due to MRC requests
      method: {{boot_method}}
      commands: nfs
      prompts:
        - 'user@apertis:'
        - '\$ '
        - '/home/user # '
      auto_login:
        login_prompt: 'apertis login:'
        username: user
        password_prompt: 'Password:'
        password: user
        login_commands:
          - sudo su

  - deploy:
      namespace: system
      timeout:
        minutes: 60
      to: usb
      device: sd-card
      tool:
        prompts: ['copying time: [0-9ms\.\ ]+, copying speed [0-9\.]+ MiB\/sec']
      images:
        image:
          url: {{baseurl}}/{{imgpath}}/{{image_date}}/{{arch}}/{{image_type}}/{{image_name}}.img.gz
        bmap:
          url: {{baseurl}}/{{imgpath}}/{{image_date}}/{{arch}}/{{image_type}}/{{image_name}}.img.bmap
      uniquify: false
      os: ubuntu
      writer:
        tool: /usr/bin/bmaptool
        options: copy {DOWNLOAD_URL} {DEVICE}
        prompt: 'bmaptool: info'

  - boot:
      namespace: system
      timeout:
        minutes: 15 # Minnowboards can spend a long time clearing memory due to MRC requests
      method: {{boot_method}}
      commands: {{boot_commands}}
      transfer_overlay:
        download_command: sudo mount -o remount,rw / ; connmand-wait-online ; cd /tmp ; busybox wget
        unpack_command: sudo tar -C / -xf
      parameters:
        shutdown-message: "reboot: Restarting system"
      auto_login:
        login_prompt: 'login:'
        username: 'user'
        password_prompt: 'Password:'
        password: user
        login_commands:
          - sudo su
      prompts:
        - '\$ '
        - '/home/user #'
        - '/tmp #'

  - test:
      timeout:
        minutes: 15
      namespace: system
      name: sanity-check
      definitions:
        - repository: https://gitlab.apertis.org/tests/apertis-test-cases.git
          revision: 'apertis/v2022dev0'
          from: git
          path: test-cases/sanity-check.yaml
          name: sanity-check
        - repository: https://gitlab.apertis.org/infrastructure/apertis-tests
          revision: 'apertis/v2022dev0'
          from: git
          path: misc/add-repo.yaml
          name: add-repo
        - repository: https://gitlab.apertis.org/tests/apertis-test-cases.git
          revision: 'apertis/v2022dev0'
          from: git
          path: test-cases/disk-rootfs-fsck.yaml
          name: disk-rootfs-fsck
