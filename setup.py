###################################################################################
# setuptools script for atc (Apertis Test Case and Renderer)
#
# Copyright (C) 2018
# Luis Araujo <luis.araujo@collabora.co.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from setuptools import setup, find_packages

setup(
    name='atc',
    version='1.1812.0',
    license='LGPL-2.1+',
    description='Apertis Test Cases Renderer',
    author='Luis Araujo',
    author_email='luis.araujo@collabora.co.uk',
    url='https://gitlab.apertis.org/tests/apertis-test-cases.git',
    packages=find_packages(),
    classifiers = [
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
    entry_points = {
        'console_scripts': [
            'atc = atc_renderer:main'
        ]
    },
    include_package_data=True,
    install_requires=['PyYaml', 'jinja2'],
    zip_safe=False,
)
